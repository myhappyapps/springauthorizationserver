package me.learn.springauthserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.ldap.LdapBindAuthenticationManagerFactory;

@Configuration
public class LdapConfig {
    @Autowired CustomLdapUserDetailsMapper customLdapUserDetailsMapper;
    public LdapConfig(LdapTemplate ldapTemplate) {
        ldapTemplate.setIgnorePartialResultException(true);
    }

    @Bean
    AuthenticationManager authenticationManager(BaseLdapPathContextSource baseLdapPathContextSource){
        LdapBindAuthenticationManagerFactory ldapBindAuthenticationManagerFactory=new LdapBindAuthenticationManagerFactory(baseLdapPathContextSource);
        //ldapBindAuthenticationManagerFactory.setUserSearchFilter("(&(objectCategory=person)(SamAccountName={0}))");
        ldapBindAuthenticationManagerFactory.setUserDnPatterns("uid={0}");
        ldapBindAuthenticationManagerFactory.setUserDetailsContextMapper(customLdapUserDetailsMapper);
        return ldapBindAuthenticationManagerFactory.createAuthenticationManager();
    }
}



