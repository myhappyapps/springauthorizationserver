package me.learn.springauthserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;

@Component
public class CustomLdapUserDetailsMapper extends LdapUserDetailsMapper implements Serializable {

    @Override
    public UserDetails mapUserFromContext(DirContextOperations dirContextOperations, String username, Collection<? extends GrantedAuthority> ldapAuthorities) {
        System.out.println("LDAP User Details: "+dirContextOperations.toString());
        return new CustomUserDetails("What rey");
    }
}